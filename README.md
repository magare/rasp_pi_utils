# **RPi EMULATOR BUILD** #

## Make project structure for embedded Linux training using Raspberry Pi ##
 Author: Sava Jakovljev

 Date: 09.02.2016.

* **Open terminal**

* **Clone Git repo**

```
#!terminal

git clone https://your_name@bitbucket.org/magare/rasp_pi_utils.git
```

* **Pokretanje skripte i bildovanje QEMU**

Pokrenuti skriptu "init.sh" pomocu terminala:

```
#!terminal

./init.sh
```

Izvrsavanje skripte traje dugo, na sva pitanja u toku zivrsavanja odgovoriti sa "y" (yes)

* **Bildovanje Linux-a**

Ovo jos uvek radi pokrenuta skripta "init.sh" iz predhodnog koraka
Citiram Savu: "*Kada vam iskoci plavi ekran to je bild konfiguracija linuxa. Sve je vec podeseno do tada, pa preko strelica na 		tastaturi idite na exit. Kada pita, stisnite enter - bice oznaceno "yes", tako i treba da bude*" Potom ce se bilovati Linux

* **Provera instaliranog**

Sada kad se sve zavrsilo, proverite da li u vasem /home direktorijumu postoji folder "dev_pi". Ukoliko postoji pomocu treminala udjite u /home/dev_pi/emulator/core. Tu bi trebalo da se nalaze skripte "start_qemu.sh" i "stop_qemu.sh". Proverite njihove dozvole za rad. Ukoliko vam ne dozvoljavaju izvrsavanje uterminal ukucajte:

```
#!terminal

chmod ugo+x start_qemu.sh
chmod ugo+x stop_qemu.sh
```

Sada skripte mogu da se izvrse.

* **Pokretanje QEMU**

QEMU se pokrece skriptom "satr_qemu.sh" tako sto ukucate u terminal:

```
#!terminal

./start_qemu.sh
```

potom bi trebalo da vam se pojavi ekran i da pocne da se dize Raspbian linux.

* **Gasenje QEMU**

Kada zavrsite sa radom, emulator se moze iskljuciti pokretanjem skripte 'stop_qemu.sh'. Ako iskljucite QEMU na 'x', posle pokrenite 'stop_qemu.sh' da bi zatvorili NFS server:

```
#!terminal

./stop_qemu.sh
```
